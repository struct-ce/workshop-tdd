# Workshop TDD

## Sobre

Repositório com os arquivos iniciais para o workshop de Test Driven Development da Struct

## Requisitos

* Saber programar
* Saber a sintaxe básica do Ruby (este tutorial [aqui](https://www.rubyguides.com/ruby-tutorial/) deve bastar)
* Ruby versão 2.5.1 instalado

## Instalação

1. Baixe o repositório
    * `git clone https://gitlab.com/struct-ce/workshop-tdd.git`
    *  Acesse https://gitlab.com/struct-ce/workshop-tdd clique no ícone de nuvem a direita mais ao topo e escolha formato de download (recomendo zip)
2. Baixe a gema bundler e instale
    * `gem install bundler`
3. Faça o bundle
    * `bundle install`